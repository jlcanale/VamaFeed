VamaFeed is an app that shows the top 100 albums currently on the iTunes Store using https://rss.applemarketingtools.com/api/v2/us/music/most-played/100/albums.json.

It implements:
* UICollectionView
* Pull To Refresh
* The Result Type
* Asynchronus Networking Code
* Codable Protocol

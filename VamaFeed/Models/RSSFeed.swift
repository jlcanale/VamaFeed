//
//  RSSAlbumFeed.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation

struct RSSFeed: Codable {
    let results: [RSSAlbum]
}

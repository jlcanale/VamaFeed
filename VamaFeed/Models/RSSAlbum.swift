//
//  RSSAlbumn.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation

public struct RSSAlbum: Codable {
    let artistName: String
    let id: String
    let name: String
    let releaseDate: String
    let kind: String
    let artistId: String?
    let artistUrl: String?
    let contentAdvisoryRating: String?
    let artworkUrl100: URL
    let genres: [RSSGenre]
    let url: String
    
    var artworkUrlFull: URL {
        let urlString = artworkUrl100.absoluteString.replacingOccurrences(of: "100x100", with: "300x300")
                
        if let url = URL(string: urlString) {
            return url
        } else {
            return artworkUrl100
        }
    }
}

//
//  RSSGenre.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation

public struct RSSGenre: Codable {
    let genreId: String
    let name: String
    let url: String
}

//
//  RSSFeedResponse.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation

struct RSSFeedResponse: Codable {
    let feed: RSSFeed
}

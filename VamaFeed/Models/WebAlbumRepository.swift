//
//  AlbumRepository.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation
import UIKit


public class WebAlbumRepository: AlbumRepository {

    private var imageCache: [URL: Data] = [:]
    
    static var top100URL = URL(string: "https://rss.applemarketingtools.com/api/v2/us/music/most-played/100/albums.json")!
    
    private func fetchDataFrom(url: URL, completion: @escaping ((Result<Data, Error>) -> Void)) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil else {
                completion(.failure(AlbumError.networkError(error?.localizedDescription ?? "")))
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(.failure(AlbumError.invalidResponse))
                return
            }
            
            guard let data = data else {
                completion(.failure(AlbumError.noDataReturned))
                return
            }

            
            switch response.statusCode {
                case 100...199:
                    //Information Response
                    completion(.failure(AlbumError.invalidResponse))
                case 200...299:
                    //Successful Reponse, parse the data
                    completion(.success(data))
                case 300...399:
                    //Server Redirect Response
                    completion(.failure(AlbumError.serverRedirect(response.statusCode)))
                case 400...499:
                    //Client Errors
                    completion(.failure(AlbumError.clientError(response.statusCode)))
                case 500...599:
                    //Server Errors
                    completion(.failure(AlbumError.serverError(response.statusCode)))
                default:
                    //Anything we don't know about now.
                    completion(.failure(AlbumError.unknownError(response.statusCode)))
            }
            
        }.resume()
    }
    
    public func getTop100(forceRefresh: Bool = false, completion: @escaping ((Result<[RSSAlbum], Error>) -> Void)) {
        
        if !forceRefresh {
            let cache = load()
            if !cache.isEmpty {
                completion(.success(cache))
                return
            }
        }
        
        fetchDataFrom(url: Self.top100URL) { result in
            switch result {
                case .success(let data):
                    //Decode
                    do {
                        let response = try JSONDecoder().decode(RSSFeedResponse.self, from: data)
                        self.save(data: data)
                        completion(.success(response.feed.results))
                    } catch {
                        completion(.failure(AlbumError.decodingError(error.localizedDescription)))
                    }
                case .failure(let error):
                    completion(.failure(error))
            }
        }
    }
    
    public func getArtwork(for url: URL, completion: @escaping (Result<UIImage, Error>) -> Void) {
        if let imageData = imageCache[url], let image = UIImage(data: imageData) {
            completion(.success(image))
        }
        
        fetchDataFrom(url: url) { result in
            switch result {
                case .success(let data):
                    guard let image = UIImage(data: data) else {
                        completion(.failure(AlbumError.imageError))
                        return
                    }
                    self.imageCache[url] = data
                    completion(.success(image))
                case .failure(let error):
                    completion(.failure(error))
            }
        }
        
        
    }
    
    private func save(data: Data) {
        guard let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first?.appendingPathComponent("albums.json") else { return }
        try? data.write(to: url)
    }
    
    private func load() -> [RSSAlbum] {
        guard let url = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first?.appendingPathComponent("albums.json") else { return [] }
        
        guard let data = try? Data(contentsOf: url) else { return [] }
        
        return (try? JSONDecoder().decode(RSSFeedResponse.self, from: data).feed.results) ?? []
    }
    
    enum AlbumError: LocalizedError {
        case invalidResponse
        case noDataReturned
        case serverRedirect(Int)
        case clientError(Int)
        case serverError(Int)
        case unknownError(Int)
        case networkError(String)
        case decodingError(String)
        case imageError
    }
}

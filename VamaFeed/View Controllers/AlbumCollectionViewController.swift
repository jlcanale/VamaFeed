//
//  AlbumCollectionViewController.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import UIKit

private let reuseIdentifier = "AlbumCell"

class AlbumCollectionViewController: UICollectionViewController {

    private var albums: [RSSAlbum] = []
    private var repository = WebAlbumRepository()
    
    private var viewModel =  AlbumCollectionViewModel()
    
    private var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Top 100 Albums"

        // Register cell classes
        self.collectionView!.register(AlbumCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.refreshControl = UIRefreshControl()
        self.collectionView.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        repository.getTop100 { result in
            self.handle(result: result)
        }
    }
    
    private func handle(result: Result<[RSSAlbum], Error>) {
        switch result {
            case .success(let albums):
                DispatchQueue.main.async { [weak self] in
                    self?.albums = albums
                    self?.collectionView.refreshControl?.endRefreshing()
                    self?.collectionView.reloadData()
                }
                
            case .failure(let error):
                print(error.localizedDescription)
        }
    }
    
    @objc private func refreshData() {
        repository.getTop100(forceRefresh: true) { result in
            self.handle(result: result)
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? AlbumCollectionViewCell else { return UICollectionViewCell() }
        // Configure the cell
        let album = albums[indexPath.row]
        
        cell.imageView.image = nil
        cell.albumTitleLabel.text = album.name
        cell.artistlabel.text = album.artistName
        
        repository.getArtwork(for: album.artworkUrl100) { result in
            switch result {
                case .success(let image):
                    DispatchQueue.main.async {
                        cell.imageView.image = image
                    }
                case .failure(_):
                    print("Error fetching Album Art")
            }
        }
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let album = albums[indexPath.row]
        let cell = collectionView.cellForItem(at: indexPath) as? AlbumCollectionViewCell
        let albumDetailViewController = AlbumDetailViewController(album: album, thumbnailImage: cell?.imageView.image ?? nil)
        navigationController?.pushViewController(albumDetailViewController, animated: true)
    }

}

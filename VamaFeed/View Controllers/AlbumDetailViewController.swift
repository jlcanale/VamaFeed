//
//  AlbumDetailViewController.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import UIKit

class AlbumDetailViewController: UIViewController {

    var album: RSSAlbum
    var thumbnailImage: UIImage?
    
    init(album: RSSAlbum, thumbnailImage: UIImage?) {
        self.album = album
        self.thumbnailImage = thumbnailImage
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.navigationItem.hidesBackButton = true
        
        //let backButton = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(dismissVC))
        //backButton.tintColor = .black
        
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        backButton.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        backButton.tintColor = .black
        backButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        backButton.layer.cornerRadius = 20
        backButton.clipsToBounds = true
        
        let blur = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        blur.frame = backButton.bounds
        blur.isUserInteractionEnabled = false
        backButton.insertSubview(blur, at: 0)
        if let imageView = backButton.imageView {
            backButton.bringSubviewToFront(imageView)
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = thumbnailImage
        
        let repository = WebAlbumRepository()
        
        if album.artworkUrl100 != album.artworkUrlFull {
            repository.getArtwork(for: album.artworkUrlFull) { result in
                switch result {
                    case .success(let image):
                        DispatchQueue.main.async {
                            imageView.image = image
                        }
                    case .failure(_):
                        print("Error fetching full image")
                }
            }
        }
        
        
        view.addSubview(imageView)
                
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor)
        ])
        
        let artistLabel = UILabel()
        artistLabel.translatesAutoresizingMaskIntoConstraints = false
        artistLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        artistLabel.textColor = .lightGray
        artistLabel.text = album.artistName
        
        view.addSubview(artistLabel)
        NSLayoutConstraint.activate([
            artistLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 8),
            artistLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8)
        ])
        
        let albumLabel = UILabel()
        albumLabel.translatesAutoresizingMaskIntoConstraints = false
        albumLabel.text = album.name
        albumLabel.font = UIFont.preferredFont(forTextStyle: .title1).bold()
        
        view.addSubview(albumLabel)
        NSLayoutConstraint.activate([
            albumLabel.topAnchor.constraint(equalTo: artistLabel.bottomAnchor),
            albumLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8)
        ])
        
        let genreLabel = PillLabel()
        genreLabel.text = "Hello"

        var genreLabels: [UIView] = []
        
        for genre in album.genres {
            let genreLabel = PillLabel()
            genreLabel.text = genre.name
            genreLabels.append(genreLabel)
        }
        
        
        let stackView = UIStackView(arrangedSubviews: genreLabels)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.spacing = 4

        view.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: albumLabel.bottomAnchor, constant: 8),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            stackView.heightAnchor.constraint(equalToConstant: 100)
        ])
        
        let albumButton = UIButton(type: .system)
        albumButton.translatesAutoresizingMaskIntoConstraints = false
        albumButton.setTitle("Visit The Album", for: .normal)
        albumButton.setTitleColor(.white, for: .normal)
        albumButton.layer.cornerRadius = 10
        albumButton.layer.backgroundColor = UIColor.systemBlue.cgColor
        albumButton.addTarget(self, action: #selector(openInStore(_:)), for: .touchUpInside)
        
        view.addSubview(albumButton)
        NSLayoutConstraint.activate([
            albumButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            albumButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            albumButton.widthAnchor.constraint(equalToConstant: 150),
            albumButton.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        
        let releaseDateLabel = UILabel()
        releaseDateLabel.translatesAutoresizingMaskIntoConstraints = false
        releaseDateLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        releaseDateLabel.textColor = .lightGray
        releaseDateLabel.textAlignment = .center
        releaseDateLabel.numberOfLines = 2
        
        var releaseDateString = ""
        if let releaseDate = dateFormatter.date(from: album.releaseDate) {
            dateFormatter.dateFormat = "MMM d, yyyy"
            releaseDateString = "Released \(dateFormatter.string(from: releaseDate))"
        } else {
            releaseDateString = "Released \(album.releaseDate)"
        }
        
        releaseDateLabel.text = "\(releaseDateString)\nCopyright 2022 Apple Inc. All Rights Reserved"
        
        view.addSubview(releaseDateLabel)
        NSLayoutConstraint.activate([
            releaseDateLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            releaseDateLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            releaseDateLabel.bottomAnchor.constraint(equalTo: albumButton.topAnchor, constant: -16)
        ])
    }
    
    
    @objc func openInStore(_ sender: UIButton!) {
        if let url = URL(string: album.url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @objc func dismissVC() {
        self.navigationController?.popViewController(animated: true)
    }
}

//
//  AlbumCollectionViewModel.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation

public class AlbumCollectionViewModel {
    var albums: [RSSAlbum] = []
    private var repository = WebAlbumRepository()
    
    
    init() {
        repository.getTop100 { result in
            switch result {
                case .success(let albums):
                    DispatchQueue.main.async { [weak self] in
                        self?.albums = albums
                    }
                    
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
    }
}

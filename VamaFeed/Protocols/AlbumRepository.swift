//
//  AlbumRepository.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import Foundation
import UIKit

public protocol AlbumRepository {
    func getTop100(forceRefresh: Bool, completion: @escaping (Result<[RSSAlbum], Error>) -> Void)
    func getArtwork(for url: URL, completion: @escaping (Result<UIImage, Error>) -> Void)
}

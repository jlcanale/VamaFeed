//
//  AlbumCollectionViewCell.swift
//  VamaFeed
//
//  Created by Joe Canale on 6/15/22.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {    
    weak var imageView: UIImageView!
    weak var albumTitleLabel: UILabel!
    weak var artistlabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        let imageView = UIImageView(frame: .zero)
        imageView.frame.size.width = frame.width
        imageView.frame.size.height = frame.height
        imageView.translatesAutoresizingMaskIntoConstraints = true
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 12
        
        let gradient = CAGradientLayer()
        gradient.frame = imageView.frame
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.6)
        gradient.endPoint = CGPoint(x: 0.0, y: 1)

        imageView.layer.insertSublayer(gradient, at: 0)
        
        
        let albumTitleLabel = UILabel()
        albumTitleLabel.font = UIFont.preferredFont(forTextStyle: .body).bold()
        albumTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        albumTitleLabel.textColor = .white
        albumTitleLabel.numberOfLines = 2
        
        let artistLabel = UILabel()
        artistLabel.font = UIFont.preferredFont(forTextStyle: .caption1)
        artistLabel.translatesAutoresizingMaskIntoConstraints = false
        artistLabel.textColor = .lightGray
        artistLabel.numberOfLines = 2

        
        let stackView = UIStackView(arrangedSubviews: [albumTitleLabel, artistLabel])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 4
        
        
        self.addSubview(imageView)
        self.addSubview(stackView)

        self.imageView = imageView
        self.albumTitleLabel = albumTitleLabel
        self.artistlabel = artistLabel

        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            imageView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
            
            stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 8),
            stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8),
            stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10),
        ])
        


        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIFont {
    func withTraits(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
    }
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
}
